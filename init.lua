-- File: /home/sergio/.nvim/config/nvim/init.lua

require('core.options')
require('core.commands')
require('core.plugins')
require('core.files')
require('core.utils')
require('core.colors')
require('core.themes')
require('core.keymaps')
require('core.autocommands')
