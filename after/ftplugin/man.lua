-- Filename: /home/sergio/.config/nvim/after/ftplugin/man.lua
-- Last Change: Wed, 09 Nov 2022 17:40:52
-- vim:set softtabstop=2 shiftwidth=2 tabstop=2 expandtab ft=lua:

vim.opt.termguicolors = true
vim.keymap.set("n", "q", "ZQ", { desc = "close help file using q"})

