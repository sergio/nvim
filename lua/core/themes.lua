-- Filename: themes.lua
-- Last Change: Tue, 08 Nov 2022 08:29:07
-- vim:set ft=lua nolist softtabstop=2 shiftwidth=2 tabstop=2 expandtab:
--

-- ████████╗██╗  ██╗███████╗███╗   ███╗███████╗███████╗
-- ╚══██╔══╝██║  ██║██╔════╝████╗ ████║██╔════╝██╔════╝
--    ██║   ███████║█████╗  ██╔████╔██║█████╗  ███████╗
--    ██║   ██╔══██║██╔══╝  ██║╚██╔╝██║██╔══╝  ╚════██║
--    ██║   ██║  ██║███████╗██║ ╚═╝ ██║███████╗███████║
--    ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚══════╝╚══════╝

local colorscheme = "ayu-dark"
local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
    print("colorscheme " .. colorscheme .. " not found!")
    return
end

-- vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
-- vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })

-- transtarent background
-- vim.cmd([[highlight normal guibg=none]])
-- vim.cmd([[highlight SignColumn  guibg=none]])
-- vim.cmd([[highlight Number guifg=none]]
