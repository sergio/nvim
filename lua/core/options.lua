-- Fname: ~/.config/nvim/lua/core/options.lua
-- Last Change: Wed, 16 Nov 2022 - 11:57:39
-- vim:set ft=lua nolist nospell softtabstop=2 shiftwidth=2 tabstop=2 expandtab:

-- main editor configs
-- vim.g.python3_host_prog = vim.loop.os_homedir() .. "/.virtualenvs/neovim/bin/python"
-- vim.loop.os_homedir() .. '/.virtualenvs/neovim/bin/python'

-- if vim.env.HOST == 'void' then
--   local python3_host_prog = vim.loop.os_homedir() .. '/.virtualenvs/neovim/bin/python'
-- else
--   local python3_host_prog = '$PREFIX/bin/python'
-- end

if vim.fn.executable("rg") then
  vim.opt.grepprg = "rg --vimgrep --no-heading --smart-case"
  vim.opt.grepformat = "%f:%l:%c:%m,%f:%l:%m"
end

if vim.fn.executable("prettier") then
  vim.opt.formatprg = "prettier --stdin-filepath=%"
end

-- set your colors in ~/.config/nvim/lua/core/themes.lua
-- local colorscheme = "material"
-- local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
-- if not status_ok then
--     print("colorscheme " .. colorscheme .. " not found!")
--     return
-- end

local global_options = {
  python3_host_prog = python3_host_prog,
  loaded_python_provier = 0,
  loaded_python3_provider = 1,
  python_host_skip_check = 1,
  python3_host_skip_check = 1,
  do_filetype_lua = 1,
  did_load_filetypes = 0,
  mapleader = ",",
  matchparen_timeout = 20,
  matchparen_insert_timeout = 20,
  tokyonight_colors = { border = "orange" }, -- specific for tokyonight colorscheme
  tokyonight_italic_comments = false,
  tokyonight_italic_keywords = false,
  ts_highlight_lua = true, -- treesiter highlight
  material_style = "deep ocean"
}

for k, v in pairs(global_options) do
  vim.g[k] = v
end

-- disable builtins plugins
local disabled_built_ins = {
  "2html_plugin",
  "getscript",
  "getscriptPlugin",
  "gzip",
  "logipat",
  "matchit",
  "netrwFileHandlers",
  "loaded_remote_plugins",
  "loaded_tutor_mode_plugin",
  -- "netrwPlugin",
  -- "netrwSettings",
  -- "netrw",
  "rrhelper",
  -- "spellfile_plugin",
  "tar",
  "tarPlugin",
  "vimball",
  "vimballPlugin",
  "zip",
  "zipPlugin",
  "matchparen", -- matchparen.nvim disables the default
}

for _, plugin in pairs(disabled_built_ins) do
  vim.g["loaded_" .. plugin] = 1
end

local options = {
  guicursor = { "n-v-c:block", "i-ci-ve:ver25", "r-cr:hor20", "o:hor50",
    "a:blinkwait300-blinkoff300-blinkon150-Cursor",
    "sm:block-blinkwait175-blinkoff150-blinkon175"
  },
  ssop = vim.opt.ssop - { "blank", "help", "buffers" } + { "terminal" },
  formatoptions = 'lnjq',
  shell = vim.env.SHELL,
  dictionary = vim.opt.dictionary + '/home/sergio/.config/nvim/words.txt',   --  " C-x C-k C-n
  jumpoptions = vim.opt.jumpoptions:append "stack",
  keywordprg = ":help",
  -- autochdir = true,
  background = 'dark',
  emoji = false,
  isfname = vim.opt.isfname:remove '=',
  undofile = true,
  shada = "!,'30,<30,s30,h,:30,%0,/30",
  fsync = true,
  list = false,
  whichwrap = vim.opt.whichwrap:append "<>[]",
  iskeyword = vim.opt.iskeyword:append "-",
  shortmess = vim.opt.shortmess:append { c = true },
  listchars = { eol = "↲", tab = "▶ ", trail = "•", precedes = "«", extends = "»", nbsp = "␣", space = "." },
  --completeopt = "menu,menuone,noselect",
  completeopt = { "menuone", "noselect"},
  encoding = "utf-8",    -- str:  String encoding to use
  fileencoding = "utf8", -- str:  File encoding to use
  syntax = "ON",        -- str:  Allow syntax highlighting
  foldenable = false,
  -- foldcolumn = "auto:9",
  foldopen = vim.opt.foldopen + "jump", -- when jumping to the line auto-open the folder
  foldmethod = "indent",
  --path = vim.opt.path + "~/.config/nvim/lua/core",
  path = vim.opt.path + ".,**",
  -- https://issuecloser.com/blog/neovim-tip-smarter-path
  -- path = table.concat(vim.fn.systemlist("fd . -td"),","),
  wildignore = { ".git", ".hg", ".svn", "*.pyc", "*.o", "*.out", "*.jpg", "*.jpeg", "*.png", "*.gif", "*.zip" },
  wildignore = vim.opt.wildignore + { "**/node_modules/**", "**/bower_modules/**", "__pycache__", "*~", "*.DS_Store" },
  wildignore = vim.opt.wildignore + { "**/undo/**", "*[Cc]ache/" },
  wildignorecase = true,
  wildmenu = true,
  infercase = true,
  lazyredraw = true,
  showmatch = true,
  switchbuf = "useopen",
  matchtime = 2,
  synmaxcol = 128, -- avoid slow rendering for long lines
  pumheight = 10,
  pumblend = 15,
  wildmode = "longest:full,full",
  timeoutlen = 1000,
  ttimeoutlen = 50, -- https://vi.stackexchange.com/a/4471/7339
  hlsearch = true, -- Highlight found searches
  ignorecase = true, -- Ignore case
  inccommand = "nosplit", -- Get a preview of replacements
  incsearch = true, -- Shows the match while typing
  joinspaces = false, -- No double spaces with join
  linebreak = true, -- Stop words being broken on wrap
  list = false, -- Show some invisible characters
  relativenumber = true,
  scrolloff = 2, -- Lines of context
  shiftround = true, -- Round indent
  shiftwidth = 4, -- Size of an indent
  expandtab = true,
  showmode = false, -- Don't display mode
  sidescrolloff = 8, -- Columns of context
  -- signcolumn = "yes:1", -- always show signcolumns
  smartcase = true, -- Do not ignore case with capitals
  smartindent = true, -- Insert indents automatically
  spell = false, -- enable spell suggestions
  spelllang = { "en_us" },
  spellsuggest = vim.opt.spellsuggest:append "8",
  splitbelow = true, -- Put new windows below current
  splitright = true, -- Put new windows right of current
  tabstop = 4, -- Number of spaces tabs count for
  termguicolors = true, -- You will have bad experience for diagnostic messages when it's default 4000.
  wrap = true,
  mouse = "a",
  undodir = vim.fn.stdpath("cache") .. "/undo",
  updatetime = 300, -- faster completion
  fillchars = {
    vert = "▕", -- alternatives │
    fold = " ",
    eob = "~", --  ~ at EndOfBuffer
    diff = "╱", -- alternatives = ⣿ ░ ─
    msgsep = "‾",
    foldopen = "▾",
    foldsep = "│",
    foldclose = "▸",
  },
  laststatus = 3,
  modelines=5,
  modeline = true,
  -- autoread = true,    -- detects file change by other apps
}

for k, v in pairs(options) do
  vim.opt[k] = v
end

local window_options = {
  numberwidth = 2,
  number = true,
  list = false,
  relativenumber = true,
  linebreak = true,
  cursorline = false,
  winblend = 0,
  foldenable = false,
  -- winbar = "%{%v:lua.require'core.winbar'.statusline()%}",
  -- winbar = "%=" .. "%m" .. " " .. "%F" .. " ",
}

for k, v in pairs(window_options) do
  vim.wo[k] = v
end

local buffer_options = {
  expandtab = true,
  softtabstop = 4,
  tabstop = 4,
  shiftwidth = 4,
  smartindent = true,
  -- suffixesadd = '.lua' -- added in lua.lua ftplugin
}

for k, v in pairs(buffer_options) do
  vim.bo[k] = v
end

vim.g.secure_modelines_allowed_items = {
  'concellevel',
  'cole',
  'textwidth',
  'tw',
  'softtabstop',
  'sts',
  'tabstop',
  'ts',
  'shiftwidth',
  'sw',
  'expandtab',
  'et',
  'noexpandtab',
  'noet',
  'filetype',
  'ft',
  'foldmethod',
  'fdm',
  'readonly',
  'ro',
  'noreadonly',
  'noro',
  'rightleft',
  'rl',
  'norightleft',
  'norl',
  'colorcolumn',
}

