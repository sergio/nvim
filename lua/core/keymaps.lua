-- ~/.config/nvim/lua/core/mappings.lua
-- Last Change: Tue, 22 Nov 2022 - 09:25:47
-- vim:set nolist softtabstop=2 shiftwidth=2 tabstop=2 expandtab ft=lua:

local function map(mode, lhs, rhs, opts)
	local options = { noremap = true, silent = true }
	if opts then
		if opts.desc then
			-- opts.desc = "kemaps.lua" .. opts.desc
			opts.desc = "keymaps.lua: " .. opts.desc
		end
		options = vim.tbl_extend('force', options, opts)
      end
	vim.keymap.set(mode, lhs, rhs, options)
end

-- source: https://www.reddit.com/r/neovim/comments/uq85hr/comment/i96whcy/
local function vim_opt_toggle(opt, on, off, name)
  local message = name
  if vim.opt[opt]:get() == off then
    vim.opt[opt] = on
    message = message .. " Enabled"
  else
    vim.opt[opt] = off
    message = message .. " Disabled"
  end
  vim.notify(message)
end

map("n", "<Down>", ":resize -5<CR>", { desc = "Resize -5" } )
map("n", "<Up>", ":resize +5<CR>", { desc = "Resize -5" } )
map("n", "<Left>", ":vertical resize -5<CR>", { desc = "Vertical Resize -5" } )
map("n", "<Right>", ":vertical resize +5<CR>", { desc = "Vertical Resize +5" } )

map("n", "<Leader>v", "<cmd>edit $MYVIMRC<CR>", { desc = "Load $MYVIMRC" })
map("n", "<Leader>z", "<cmd>edit ~/.zshrc<CR>", { desc = "Load ~/.zshrc" })

map("n", "<Leader>q", "ZQ<CR>", { desc = "exit neovim"} )

-- nmap <expr> <leader>v (len(getbufinfo({'buflisted':1})) > 1 ? ':drop $MYVIMRC<CR>' : ':edit $MYVIMRC<CR>')

-- jump between splits
map("n", "<c-m-h>", "<c-w>h", { desc = "jump to the left split"})
map("n", "<c-m-l>", "<c-w>l", { desc = "jump to the right split"})
map("n", "<c-m-k>", "<c-w>k", { desc = "jump to the top split"})
map("n", "<c-m-j>", "<c-w>j", { desc = "jump to the bottom split"})

-- Reselect visual when indenting
map("v", "<", "<gv", { desc = "Indent in visual mode and keep visual selection"})
map("v", ">", ">gv", { desc = "Indent in visual mode and keep visual selection"})
map("x", ">", ">gv", { desc = "Indent in visual mode and keep visual selection"})
map("x", "<", "<gv", { desc = "Indent in visual mode and keep visual selection"})

-- move lines
map('n', '<M-j>', [[:m .+1<CR>==]], { desc = "move lines down easily" })
map('n', '<M-k>', [[:m .-2<CR>==]], { desc = "move lines up easily" })
map('i', '<M-j>', [[:<Esc>:m .+1<CR>==gi]], { desc = "move lines down easily" })
map('i', '<M-k>', [[:<Esc>:m .-2<CR>==gi]], { desc = "move lines up easily" })
map('v', '<M-j>', [[:m '>+1<CR>gv=gv]], { desc = "move lines down easily" })
map('v', '<M-k>', [[:m '<-2<CR>gv=gv]], { desc = "move lines up easily" })

-- map('n', 'n', 'nzz:lua require("core.utils").flash_cursorline()<CR>Nn', { desc = "move the cursor for the center on n"})
-- map('n', 'N', 'Nzz:lua require("core.utils").flash_cursorline()<CR>nN', { desc = "move the cursor for the center on N"})
-- map('n', '*', '*N:lua require("core.utils").flash_cursorline()<CR>zz', { desc = "move the cursor for the center on *"})
-- map('n', '#', '#N:lua require("core.utils").flash_cursorline()<CR>zz', { desc = "move the cursor for the center on #"})
-- map('n', '<c-o>', '<c-o>:lua require("core.utils").flash_cursorline()<CR><CR>')
-- map('n', '<c-i>', '<c-i>:lua require("core.utils").flash_cursorline()<CR><CR>')

map('n', '<c-o>', '<c-o>zz:lua require("specs").show_specs()<CR>', { desc = "backward jump (cursor centered zz)"})
map('n', '<c-i>', '<c-i>zz:lua require("specs").show_specs()<CR>', { desc = "forward jump (cursor centered zz)"})
map('n', 'n', 'nzz:lua require("specs").show_specs()<CR>', { desc = "move the cursor for the center on n"})
map('n', 'N', 'Nzz:lua require("specs").show_specs()<CR>', { desc = "move the cursor for the center on N"})
map('n', '*', '*N:lua require("specs").show_specs()<CR>', { desc = "move the cursor for the center on *"})
map('n', '#', '#N:lua require("specs").show_specs()<CR>', { desc = "move the cursor for the center on #"})

-- map('n', '<F5>', '<cmd>echo teste<cr>', { desc = 'mapeamento de teste'})
map({'n', 'i'}, '<F6>', function()
    require('core.colors').choose_colors()
    end,
    { desc = "Choose some colorschemes"}
)

-- Nvim Tree
-- map("n", "<leader>e", ":PackerLoad nvim-tree.lua | NvimTreeToggle<CR>", { silent = true })
-- map("n", "<leader>e", ":PackerLoad nvim-tree.lua<cr>:NvimTreeToggle<CR>", { silent = true })
--map("n", "<leader>e", ":NvimTreeToggle<CR>", { silent = true })
map("n", "<F11>", ":PackerLoad nvim-tree.lua<cr>:NvimTreeFindFile<CR>", { silent = true })

-- surround mappings:
map("n", "<leader>'", [[:normal ysiw'<CR>]], { desc = "surround inner word"})
map('n', '<leader>"', [[:normal ysiw"<CR>]], { desc = "surround inner word"})
map('n', '<leader>1', [[:normal ysiw"<CR>]], { desc = "surround inner word"})
map("n", "<leader>2", [[:normal ysiw'<CR>]], { desc = "surround inner word"})
map("n", "<leader>3", [[:normal ysiw`<CR>]], { desc = "surround inner word"})

vim.keymap.set('n', '<A-i>', '<CMD>lua require("FTerm").toggle()<CR>')
vim.keymap.set('t', '<A-i>', '<C-\\><C-n><CMD>lua require("FTerm").toggle()<CR>')

-- jump to the last changed spot
map("n", "gl", "`.zz", { desc = "Jump to the last change in the file"})
map("n", "<leader>i", "`^zz", { desc = "Jump to the last insert location but in normal mode"})

-- copy to the primary selection on mouse release
map("v", "<LeftRelease>", '"*y' , {silent = true})

-- I have set --> export HOST=android on termux
if vim.env.HOST == 'android' then
  -- map("n", "<leader>p", "'`[' . strpart(getregtype(), 0, 1) . '`]'", { expr = true })
  map('n', '<leader>p', ':r!termux-clipboard-get<CR>', { desc = "paste yank register using termux-clipboard-get" })
  map('n', '<leader>y', ':!termux-clipboard-set<CR>', { desc = "yank using termux-clipboard-set" })
else

  map('v', '<leader>y', '"+y:echo "Selection copied to +"<CR>', { desc = "copy to clipboard +" })
  map('v', '<leader>Y', '"*y:echo "Selection copied to *"<CR>', { desc = "copy to primary selection *" })

  map({"n","v"}, '<leader>p', '"+gp', { desc = "paste clipboard" })
  map({"n","v"}, '<leader>P', '"*gp', { desc = "paste primary selection" })

end

-- Reference: https://vi.stackexchange.com/a/9593/7339
map("n", "'", "`", {desc = "Jump to exact mark"})
map("n", "`", "'", {desc = "Jump to exact mark"})

map("n", "<c-p>", [[<cmd>lua require('core.files').search_dotfiles()<cr>]], { silent = true, desc = "search dotfiles" })
map("i", "<LeftMouse>", '<C-o>:normal "*gp<CR>', { desc = "Use left mouse to paste primary selection in insert mode"})
map("i", "<RightMouse>", '<C-o>:normal "+gp<CR>', { desc = "Use right mouse to paste clipboard + in insert mode"})

---- line text-objects (inner and whole line text-objects)
---- I am trying now to create some "inner next object", "around last object" and
---- these mappings conflict with the mappings bellow, so, I am disabling those for a while
map("x", "al", ":<C-u>norm! 0v$<cr>", { desc = "Line text object" })
map("x", "il", ":<C-u>norm! _vg_<cr>", { desc = "Line text object" })
map("o", "al", ":<C-u>norm! 0v$<cr>", { desc = "Line text object" })
map("o", "il", ":<C-u>norm! _vg_<cr>", { desc = "Line text object" })

-- Fast saving with <leader> and s
map({ 'n', 'v' }, '<F9>', ':update!<CR>', { desc = "Update file to disc" })
map({ 'n', 'v' }, '<c-s>', ':update!<CR>', { desc = "Update file to disc" })
map('i', '<F9>', '<C-o>:update!<CR>', { desc = "Update file to disc" })

-- Update Plugins
map("n", "<Leader>u", ":PackerSync<CR>", { desc = "Update plugins using PackerSync"})

-- harpoon: https://github.com/ThePrimeagen/harpoon
-- ~/.config/nvim/after/plugin/pl-file-exp/harpoon.lua
map("n", "<leader>m", "<cmd>lua require('harpoon.mark').add_file(vim.fn.expand('%:p'))<CR>", { desc = "Add current file to the harpoon list"})
map("n", "<leader>a", "<cmd>lua require('harpoon.ui').toggle_quick_menu()<CR>", { desc = "Add current file to the harpon list"})
map("n", "<leader>j", "<cmd>lua require('harpoon.ui').nav_next()<CR>", { desc = "Show next file on harpoon"})
map("n", "<leader>k", "<cmd>lua require('harpoon.ui').nav_prev()<CR>", { desc = "Show previous file on harpoon"})
map("n", "<A-1>", function() require("harpoon.ui").nav_file(1) end, { desc = "harpoon file 1"})
map("n", "<A-2>", function() require("harpoon.ui").nav_file(2) end, { desc = "harpoon file 2"})
map("n", "<A-3>", function() require("harpoon.ui").nav_file(3) end, { desc = "harpoon file 3"})
map("n", "<A-4>", function() require("harpoon.ui").nav_file(4) end, { desc = "harpoon file 4"})

-- make clipboard register blockwise
map('n', '<F3>', '<cmd>lua require("core.utils").blockwise_register()<CR>', { desc = 'turn clipboard blockwise' })
map('n', '<S-F3>', '<cmd>lua require("core.utils").blockwise_register("0")<CR>', { desc = 'turn yank register blockwise' })
map('n', '<leader>h', '<cmd>lua require("core.utils").changeheader()<CR>', { desc = 'change file header (last change)'})

-- Easier file save
map("n", "<Delete>", "<cmd>:update!<CR>")
map("n", "<F9>", "<cmd>update<cr>")
map("i", "<F9>", "<c-o>:update<cr>")

-- chama a ajuda via Telescope
-- map('n', '<C-M-h>', ':Telescope help_tags<CR>')
map("n", "<C-M-o>", ':lua require("core.files").search_oldfiles()<CR>') -- already mapped on which-key

map({ "n", "v" }, '<F2>', function()
    vim_opt_toggle("relativenumber", true, false, "Relative number")
   end, { desc = "toggle relative number"})

map('n', '<leader>d', '<cmd>lua require("core.utils").squeeze_blank_lines()<cr>')

-- alternate file mapping (add silent true)
map('n', '<bs>',
[[:<c-u>exe v:count ? v:count . 'b' : 'b' . (bufloaded(0) ? '#' : 'n')<cr>]],
{ silent = true, noremap = true } )

vim.keymap.set( "n", "dd", function()
  if vim.api.nvim_get_current_line():match("^%s*$") then
    return '\"_dd'
  else
    return 'dd'
  end
end, { noremap = true, expr = true } )

-- substitute plugin - sxiw
-- map("n", "s", "<cmd>lua require('substitute').operator()<cr>", { noremap = true })
map("n", "ss", "<cmd>lua require('substitute').line()<cr>", { noremap = true })
-- map("n", "S", "<cmd>lua require('substitute').eol()<cr>", { noremap = true })
-- map("x", "s", "<cmd>lua require('substitute').visual()<cr>", { noremap = true })
map("n", "sx", "<cmd>lua require('substitute.exchange').operator()<cr>", { noremap = true })
map("n", "sxx", "<cmd>lua require('substitute.exchange').line()<cr>", { noremap = true })
map("x", "X", "<cmd>lua require('substitute.exchange').visual()<cr>", { noremap = true })
map("n", "sxc", "<cmd>lua require('substitute.exchange').cancel()<cr>", { noremap = true })

map('n', '<C-M-y>', "<cmd>%y+<CR>", { desc = 'Copy the whole file to the clipboard', silent = true })

-- in termux up+8 = F8
map("n", "<F8>", [[<cmd>lua require("core.files").xdg_config()<cr>]], { silent = true })


-- shortcuts to jump in the command line
vim.keymap.set("c", "<C-a>", "<Home>", { desc = "jump to the beginning of command line"})
vim.keymap.set("c", "<C-e>", "<End>" , { desc = "jump to the end of command line"})

map("i", "<leader>a", "<c-o>_", { desc = "jump to the beginning of line in inserte mode"})
map("i", "<leader>e", "<c-o>$" , { desc = "jump to the end of linein inserte mode"})

-- map('c', '<c-k>', [[<c-\>egetcmdline()[:getcmdpos()-2]<CR>]], { desc = "delete the rest of command line"})

map('c', '<C-j>', 'pumvisible() ? "\\<C-n>" : "\\<C-j>"', { expr = true, desc = "complete with c-j"})
map('c', '<C-k>', 'pumvisible() ? "\\<C-p>" : "\\<C-k>"', { expr = true, desc = "complete with c-k"})
map('c', 'w!!', '!sudo tee "%"', { desc = "sudo write"})

-- I don't know why my map helper does not work in this case
vim.keymap.set("n", "ç", "<Esc>:", { desc = 'type ç to get to the command line'})
vim.keymap.set("c", "çç", "<Esc>", { desc = 'type ç to get to the command line'})
vim.keymap.set("n", "<Space>", "/", { desc = 'Type space to start searching'})
-- vim.keymap.set("n", "<Leader>f", "<cmd>Pounce<cr>", { desc = 'Fuzzy search in the file'})

-- -- leap plugin
-- vim.keymap.set({'n', 'x', 'o'}, '<leader>f', '<Plug>(leap-forward-to)')
-- vim.keymap.set({'n', 'x', 'o'}, '<leader>F', '<Plug>(leap-backward-to)')
-- vim.keymap.set({'n', 'x', 'o'}, '<leader>t', '<Plug>(leap-forward-till)')
-- vim.keymap.set({'n', 'x', 'o'}, '<leader>T', '<Plug>(leap-back-till)')

-- Make Y yank to end of the line
map("n", "Y", "yg_", { desc = "copy until the end of line"})

map( "n", "<C-l>", [[ (&hls && v:hlsearch ? ':nohls' : ':set hls')."\n" <BAR> redraw<CR>]],
   { silent = true, expr = true, desc = "toggles highlighting" })

-- source: https://www.vi-improved.org/vim-tips/
map("n", "j", [[v:count ? (v:count > 5 ? "m'" . v:count : '') . 'j' : 'gj']],
    { expr = true, desc = "Adds jumps greater than five lines to the jumplist" })
map("n", "k", [[v:count ? (v:count > 5 ? "m'" . v:count : '') . 'k' : 'gk']],
   { expr = true , desc = "Add jumps greater than five lines to the jumplist"})

map({"n", "i"}, "<leader>l", function()
  vim_opt_toggle("list", true, false, "List hidden chars")
end, { desc = "Toggle list hidden chars"})

map("n", "<C-M-y>", ":%y+<CR>", { desc = "Copy the whole file to the clipboard", silent = true})

-- :nnoremap <silent> J :let p=getpos('.')<bar>join<bar>call setpos('.', p)<cr>
map("n", "J", function()
    local position = vim.fn.getpos('.')
    vim.cmd('join')
    vim.fn.setpos('.', position)
end,
  { desc = "Join lines keeping cursor position"}
)

-- :nnoremap <silent> J :let p=getpos('.')<bar>join<bar>call setpos('.', p)<cr>
map("n", "gJ", function()
    local position = vim.fn.getpos('.')
    vim.cmd [[sil! keepp keepj .s/\n//g]]
    vim.fn.setpos('.', position)
end,
  { desc = "Join lines keeping cursor position"}
)

map("n", "<F4>", [['`[' . strpart(getregtype(), 0, 1) . '`]']], { expr = true, desc = "Select your pasted or changed text" })

-- quickfix mappings
map('n', '[q', ':cprevious<CR>')
map('n', ']q', ':cnext<CR>')
map('n', ']Q', ':clast<CR>')
map('n', '[Q', ':cfirst<CR>')

vim.keymap.set({"n", "i"}, "<leader>s", function()
  vim.opt_local.spell = not(vim.opt_local.spell:get())
  print("spell: " .. tostring(vim.o.spell))
end, { desc="Toggle spell"} )

-- better gx mapping
-- https://sbulav.github.io/vim/neovim-opening-urls/
map("", "gx", '<Cmd>call jobstart(["xdg-open", expand("<cfile>")], {"detach": v:true})<CR>', { desc = "Improved gx"})
map("n", "<CR>", "gf", { desc = "open file under ther cursor"})

-- discard buffer
-- fixing a temporary issue: https://github.com/dstein64/nvim-scrollview/issues/10
-- famiu/bufdelete.nvim
map("n", "<leader>x", ":wsh | up | sil! bdelete<cr>", { silent = true, desc = "Close current buffer" })
map("n", "<leader>n", ":wsh | up | sil! enew<cr>:bd #<cr>", { silent = true, desc = "Close buffer but keep window"})
map("n", "<leader>w", ":bwipeout!<cr>", { silent = true, desc = "Discard current buffer" })
map("n", "<C-c>", ":new|bd #<CR>", { silent = true, desc = "close buffer without loosing the opened window" })

-- Break undo chain on punctuation so we can
-- use 'u' to undo sections of an edit
local break_points = {',', '.', '!', '?', ';', '(', ':', '[', '{', '/'}
for _, char in ipairs(break_points) do
  map('i', char, char .. "<C-g>u", { noremap = true })
end

map('i', '<c-k>', '<C-o>D', {desc = 'Delete the rest of the line'})

-- avoid clipboard hacking security issue
-- http://thejh.net/misc/website-terminal-copy-paste
-- inoremap <C-R>+ <C-r><C-o>+
map("i", "<C-r>+", "<C-r><C-o>+", { desc = 'fix terminal copy paste hack issue' })
map('i', '<C-r>*', '<C-r><C-o>*', { desc = 'fix terminal copy paste hack issue' })
map('i', '<C-r>"', '<C-r><C-o>"', { desc = 'Pastes default register literally allowing you to dot repeat' })
map("i", "<S-Insert>", "<C-r><C-o>*", { desc = 'fix terminal copy paste hack issue' })

-- tnoremap <expr> <Esc> &ft == 'fzf' ? '<Esc>' : '<C-\><C-n>'
map('t', '<Esc>', [[&ft == 'fzf' ? '<Esc>' : '<C-\><C-n>']], { expr = true, desc = "avoid remaping esc in terminal"})

-- https://vi.stackexchange.com/a/18895/7339
-- nnoremap <silent> c<Tab> :let @/=expand('<cword>')<cr>cgn
map("n", "<leader>*", [[*<c-o>cgn]], { silent = true, desc = "substitute current word with cgn"})
map("n", "<leader>#", [[#<c-o>cgN]], { silent = true, desc = "substitute current word with cgn"})

-- Alt + l to jump outside autopairs:
map('i', '<M-l>', '<Right>')
map('i', '<M-h>', '<Left>')

-- Nvim Tree
-- map("n", "<leader>e", ":PackerLoad nvim-tree.lua | NvimTreeToggle<CR>", { silent = true })
-- map("n", "<leader>e", ":PackerLoad nvim-tree.lua<cr>:NvimTreeToggle<CR>", { silent = true })
--map("n", "<leader>e", ":NvimTreeToggle<CR>", { silent = true })
-- map("n", "<F11>", ":PackerLoad nvim-tree.lua<cr>:NvimTreeFindFile<CR>", { silent = true })

-- Reference: https://stackoverflow.com/a/73290052/2571881
vim.keymap.set("n", "<leader>e", function()
	local nt_status, _ = pcall(require, "nvim-tree")
	if not nt_status then
    return ":Ex<CR>"
  else
    return ":NvimTreeToggle<CR>"
  end
end,
  { desc = "File explorer" , expr = true, replace_keycodes = true}
)


vim.keymap.set("n", "<leader>b", function()
	local status_ok, _ = pcall(require, "buffer_manager")
  if not status_ok then
    return ':ls<CR>:b<space>'
  else
    return ":lua require('buffer_manager.ui').toggle_quick_menu()<cr>"
  end
	end,
  { desc = "List opened buffers" , expr = true, replace_keycodes = true}
)

-- vim.keymap.set("n", "<leader>b", function()
--   local status_ok, buffer_manager = pcall(require, "buffer_manager")
--   if not status_ok then
--     vim.cmd([[:ls<CR>:b<space>]]) -- double check this one
--   else
--     return buffer_manager.ui.toggle_quick_menu()
-- end
-- end,
-- { desc = "List opened buffers" } )

local function buff_nav()
	local nt_status, _ = pcall(require, "bufferline")
	if not nt_status then
		map("n", "<M-,>", "<CMD>bprevious<CR>", { desc = "jump to the previous buffer"})
		map("n", "<M-.>", "<CMD>bnext<CR>", { desc = "jump to the next buffer"})
	end
    map("n", "<M-,>", "<CMD>BufferLineCyclePrev<CR>", { desc = "jump to the previous buffer"})
    map("n", "<M-.>", "<CMD>BufferLineCycleNex<CR>", { desc = "jump to the next buffer"})
end

buff_nav()

-- Tagbar
map('n', '<leader>z', ':TagbarToggle<CR>') -- open/close
