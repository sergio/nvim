-- vim:set ft=lua nolist softtabstop=2 shiftwidth=2 tabstop=2 expandtab:
--
-- ██████╗ ██╗     ██╗   ██╗ ██████╗ ██╗███╗   ██╗███████╗
-- ██╔══██╗██║     ██║   ██║██╔════╝ ██║████╗  ██║██╔════╝
-- ██████╔╝██║     ██║   ██║██║  ███╗██║██╔██╗ ██║███████╗
-- ██╔═══╝ ██║     ██║   ██║██║   ██║██║██║╚██╗██║╚════██║
-- ██║     ███████╗╚██████╔╝╚██████╔╝██║██║ ╚████║███████║
-- ╚═╝     ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝╚═╝  ╚═══╝╚══════╝

local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system({
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	})
	print("Installing packer close and reopen Neovim...")
	vim.cmd([[packadd packer.nvim]])
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]])

-- Use a protected call so we don't error out on first use
local status, packer = pcall(require, "packer")
if not status then
	return
end

-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

-- Install your plugins here
return packer.startup(function(use)
	--
	-- ██▄ ▄▀▄ ▄▀▀ ██▀
	-- █▄█ █▀█ ▄██ █▄▄

	use("wbthomason/packer.nvim")
	use("nvim-lua/plenary.nvim")
	use("nvim-tree/nvim-web-devicons")

	-- █▀ █ █   ██▀   ██▀ ▀▄▀ █▀▄ █   ▄▀▄ █▀▄ ██▀ █▀▄ ▄▀▀
	-- █▀ █ █▄▄ █▄▄   █▄▄ █ █ █▀  █▄▄ ▀▄▀ █▀▄ █▄▄ █▀▄ ▄██

	-- nvim tree
	use({"nvim-tree/nvim-tree.lua",
    config = function()
      require("nvim-tree").setup()
    end,
  })

	-- telescope
	use({"nvim-telescope/telescope.nvim",
    config = function()
      require('plugins_settings.telescope')
    end,
  })

	-- telescope fzf
	use({ "nvim-telescope/telescope-fzf-native.nvim", run = "make" })

	-- -- telescope projects
	-- use("nvim-telescope/telescope-project.nvim")

  -- the primagen
  use "ThePrimeagen/harpoon"

	-- ▄▀▀ █▄ █ █ █▀▄ █▀▄ ██▀ ▀█▀ ▄▀▀   ▄▀▄ █▀▄ █▄ █   ▄▀▀ ▄▀▄ █▄ ▄█ █▀▄ █   ██▀ ▀█▀ █ ▄▀▄ █▄ █
	-- ▄██ █ ▀█ █ █▀  █▀  █▄▄  █  ▄██   █▀█ █▄▀ █ ▀█   ▀▄▄ ▀▄▀ █ ▀ █ █▀  █▄▄ █▄▄  █  █ ▀▄▀ █ ▀█

	-- autocompletion
	use({"hrsh7th/nvim-cmp",
     config = function()
      require('plugins_settings.cmp')
    end,
  })
	use("hrsh7th/cmp-buffer")
	use("hrsh7th/cmp-path")
	-- use("hrsh7th/cmp-cmdline")

	-- snippets
	use("L3MON4D3/LuaSnip")
	use("saadparwaiz1/cmp_luasnip")
	use("rafamadriz/friendly-snippets")

	-- manage & install lsp servers, linters & formatters
	use("williamboman/mason.nvim")
	use("williamboman/mason-lspconfig.nvim")

	-- configuring lsp servers
	use("neovim/nvim-lspconfig")
	use("hrsh7th/cmp-nvim-lsp")

	use({"glepnir/lspsaga.nvim",
    config = function()
        require('lspsaga').setup({})
    end,
  })

	use("jose-elias-alvarez/typescript.nvim")
	use("onsails/lspkind.nvim")

	-- formatting & linting
	use("Jose-elias-alvarez/null-ls.nvim")
	use("jayp0521/mason-null-ls.nvim")

	-- treesitter
	use({
		"nvim-treesitter/nvim-treesitter",
		run = ":TSUpdate",
	})

  use{"nvim-treesitter/nvim-treesitter-textobjects", after = { 'nvim-treesitter' }}

	-- █ █ ▀█▀ █ █   ▄▀▀
	-- ▀▄█  █  █ █▄▄ ▄██

  -- fix cursorhold bug
  use "antoinemadec/FixCursorHold.nvim"

  -- automatic commits - git
  -- use('October-Studios/autocommit.nvim')

  -- file type detection
  use("nathom/filetype.nvim")

	-- nvim comment
	use("numToStr/Comment.nvim")

	-- auto closing
	use({"windwp/nvim-autopairs",
    require("nvim-autopairs").setup {}
  })
	use("windwp/nvim-ts-autotag")

	-- nvim surround
	use("kylechui/nvim-surround")

	-- git signs
	use("lewis6991/gitsigns.nvim")

	-- git
	use("dinhhuy258/git.nvim")

	-- impatient nvim
	use("lewis6991/impatient.nvim")

	-- indent line
	use("lukas-reineke/indent-blankline.nvim")

  use({
    "gbprod/substitute.nvim",
    config = function()
      require("substitute").setup({
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      })
    end
  })

  use {"j-morano/buffer_manager.nvim"}

  -- https://www.youtube.com/watch?v=2KLFjhGjmbI
  -- https://github.com/ggandor/leap.nvim
  -- use "ggandor/leap.nvim"

	-- terminal
	use("numToStr/FTerm.nvim")

	-- █ █ █ ▄▀▀ █ █ ▄▀▄ █
	-- ▀▄▀ █ ▄██ ▀▄█ █▀█ █▄▄

-- smooth scrolling
  use ({'karb94/neoscroll.nvim',
    config = function()
      require('user.neoscroll')
    end,
  })
  -- use { 'gen740/SmoothCursor.nvim',
  --   config = function()
  --     require('smoothcursor').setup()
  --   end
  -- }
  use ({'edluffy/specs.nvim',
    config = function()
      require('plugins_settings.specs')
    end,
  })  -- Show where your cursor moves when jumping

  -- use("gelguy/wilder.nvim")
  -- use({ "romgrk/fzy-lua-native", run = "make" })

	-- statusline
	use({"nvim-lualine/lualine.nvim",
    config = function()
      require('plugins_settings.lualine')
    end,
  })

	-- bufferline
	use({"akinsho/bufferline.nvim",
    config = function()
      require('plugins_settings.bufferline')
    end,
  })

	-- colorizer
	use("norcalli/nvim-colorizer.lua")

  -- -- dims your inactive windows
  -- use "sunjon/shade.nvim"
  -- use "andreadev-it/Shade.nvim"
  use("levouh/tint.nvim")
  -- command preview
  use {
    "smjonas/live-command.nvim",
    -- live-command supports semantic versioning via tags
    -- tag = "1.*",
    config = function()
      require("live-command").setup {
        commands = {
          Norm = { cmd = "norm" },
          G = { cmd = "g" },
        },
      }
    end,
  }

	-- dashboard
	-- use("goolord/alpha-nvim")

	-- THEMES
	use "folke/tokyonight.nvim"
  use "navarasu/onedark.nvim"
  use "ellisonleao/gruvbox.nvim"
  use "tanvirtin/monokai.nvim"
  use({ 'rose-pine/neovim', as = 'rose-pine', })
  use "Shatur/neovim-ayu"
  use { "bluz71/vim-moonfly-colors", as = "moonfly" }
  use "EdenEast/nightfox.nvim"
  use 'marko-cerovac/material.nvim'
  use 'Yazeed1s/oh-lucy.nvim'
  use 'neanias/everforest-nvim'

  use {
    "catppuccin/nvim",
    as = "catppuccin",
    config = function()
      require("catppuccin").setup {
          flavour = "macchiato" -- mocha, macchiato, frappe, latte
      }
      -- vim.api.nvim_command "colorscheme catppuccin"
    end
  }

	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)
