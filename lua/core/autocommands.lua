-- File: /home/.config/nvim/lua/core/autocmds.lua
-- Last Change: Tue, 15 Nov 2022 - 09:49:55
-- vim:set ft=lua softtabstop=2 shiftwidth=2 tabstop=2 expandtab nolist:

local fn = vim.fn

local augroups = {}

augroups.buf_write_pre = {

  mkdir_before_saving = {
    event = {"BufWritePre", "FileWritePre"},
    pattern = "*",
    callback = function(ctx)
      vim.fn.mkdir(vim.fn.fnamemodify(ctx.file, ':p:h'), 'p')
    end,
  },

  -- update_file_header = {
  --   event = { "BufWritePre" },
  --   pattern = "*",
  --   command = "lua require('core.utils').changeheader()",
  -- }

}

augroups.insert = {

  clear_search_highlighting = {
    event = "InsertEnter",
    pattern = "*",
    callback = function()
      vim.opt_local.hlsearch = false
      vim.fn.clearmatches()
    end,
  },

  center_on_insert = {
    event = "InsertEnter",
    pattern = "*",
    command = "normal zz",
  },

  match_extra_spaces = {
    event = "InsertLeave",
    pattern = "*",
    command = [[
    highlight RedundantSpaces ctermbg=red guibg=red
    match RedundantSpaces /\s\+$/
    ]]
  },

  disable_paste = {
    event = "InsertLeave",
    pattern = "*",
    command = "set nopaste",
  }

}

augroups.misc = {

  --Packer_Update = {
  --  event = "BufWritePost",
  --  pattern = "packer_init.lua",
  --  callback = function()
  --    vim.cmd [[source <afile> | PackerSync]]
  --  end,
  --},

  trim_extra_spaces_and_newlines = {
    event = "BufWritePre",
    pattern = "*",
    callback = function()
      require("core.utils").preserve('%s/\\s\\+$//ge')
    end,
  },

  unlist_terminal = {
    event = "TermOpen",
    pattern = "*",
    command = [[
    tnoremap <buffer> <Esc> <c-\><c-n>
    tnoremap <buffer> <leader>x <c-\><c-n>:bd!<cr>
    tnoremap <expr> <A-r> '<c-\><c-n>"'.nr2char(getchar()).'pi'
    startinsert
    nnoremap <buffer> <C-c> i<C-c>
    setlocal listchars= nonumber norelativenumber
    setlocal nobuflisted
    ]],
  },

  fix_commentstring = {
    event = "BufEnter",
    pattern = "*config,*rc,*conf,sxhkdrc,bspwmrc",
    callback = function()
      vim.bo.commentstring = "#%s"
      vim.bo.filetype = "config"
      -- vim.cmd('set syntax=config')
    end,
  },

  reload_sxhkd  = {
    event = "BufWritePost",
    pattern = "sxhkdrc",
    command = [[!pkill -USR1 -x sxhkd]],
  },

  make_scripts_executable = {
    event = "BufWritePost",
    pattern = "*.sh,*.py,*.zsh",
    callback = function()
      local file = vim.fn.expand("%p")
      local status = require('core.utils').is_executable()
      if status ~= true then
        vim.fn.setfperm(file, "rwxr-x---")
      end
    end
  },

  update_xresources = {
    event = "BufWritePost",
    pattern = "~/.Xresources",
    command = [[!xrdb -merge ~/.Xresources]],
  },

  updated_xdefaults = {
    event = "BufWritePost",
    pattern = "~/.Xdefaults",
    command = [[!xrdb -merge ~/.Xdefaults]],
  },

  restore_cursor_position = {
    event = "BufRead",
    pattern = "*",
    callback = function()
      if vim.bo.filetype == "gitcommit" then
        return
      end
      if fn.line("'\"") > 0 and fn.line("'\"") <= fn.line("$") then
        fn.setpos('.', fn.getpos("'\""))
        vim.api.nvim_feedkeys('zz', 'n', true)
      end
    end
  },

  -- auto_chdir = {
  --   event = "BufEnter",
  --   pattern = "*",
  --   command = [[silent! lcd %:p:h]]
  -- },

  lwindow_quickfix = {
    event = "QuickFixCmdPost",
    pattern = "l*",
    command = [[lwindow | wincmd j]],
  },

  cwindow_quickfix = {
    event = "QuickFixCmdPost",
    pattern = "[^l]*",
    command = [[cwindow | wincmd j]],
  },
}

-- augroups.toggle_cursorline = {
--
--   enable_cursorline = {
--     event = { "VimEnter","WinEnter","BufWinEnter" },
--     pattern = "*",
--     callback = function()
--       vim.wo.cursorline = true
--     end,
--   },
--
--   disable_cursorline = {
--     event = "WinLeave",
--     pattern = "*",
--     callback = function()
--       vim.wo.cursorline = false
--     end,
--   },
--
-- }

augroups.resize_window = {

  winresize = {
    event = { "VimResized", "WinEnter" },
    pattern = "*",
    command = "wincmd =",
  },

}

augroups.yankpost = {

  save_cursor_position = {
    event = { "VimEnter", "CursorMoved" },
    pattern = "*",
    callback = function()
      Cursor_pos = vim.fn.getpos('.')
    end,
  },

  highlight_yank = {
    event = "TextYankPost",
    pattern = "*",
    callback = function ()
      vim.highlight.on_yank{higroup="IncSearch", timeout=400, on_visual=true}
      print("Selection copied")
    end,
  },

  yank_restore_cursor = {
    event = "TextYankPost",
    pattern = "*",
    callback = function()
      local cursor = vim.fn.getpos('.')
      if vim.v.event.operator == 'y' then
        vim.fn.setpos('.', Cursor_pos)
      end
    end,
  },

}

for group, commands in pairs(augroups) do
  local augroup = vim.api.nvim_create_augroup("AU_"..group, {clear = true})

  for _, opts in pairs(commands) do
    local event = opts.event
    opts.event = nil
    opts.group = augroup
    vim.api.nvim_create_autocmd(event, opts)
  end
end

