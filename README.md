## Para clonar esse repô:

    git clone https://sergio@bitbucket.org/sergio/mynvim.git ~/.config/nvim

No meu caso como possuo a chave ssh desse repô em particular:

    git clone git@bitbucket.org:sergio/mynvim.git ~/.config/nvim
